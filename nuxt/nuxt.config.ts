// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: {
    enabled: true,

    timeline: {
      enabled: true
    }
  },
  nitro: {
    compressPublicAssets: {
      gzip: true,
      brotli: true
    },
  },
})
